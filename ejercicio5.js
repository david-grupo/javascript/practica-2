var text;
var a;
var e;
var i;
var o;
var u;
var vocales = [];
var esp;
var largo;
var efect;

var ab, eb, ib, ob, ub;

function accionar() {
    text = document.querySelector("#texto").value;
    a = text.match(/a/gi);
    console.log(a);
    e = text.match(/e/gi);
    i = text.match(/i/gi);
    console.log(i);
    o = text.match(/o/gi);
    u = text.match(/u/gi);
    vocales = [a, e, i, o, u];

    esp = text.match(/ /gi);
    largo = text.length;
    efect = largo - esp.length;
    if (a == null) { ab = 0 } else { ab = parseInt((a.length * 100) / efect); }
    if (e == null) { eb = 0 } else { eb = parseInt((e.length * 100) / efect); }
    if (i == null) { ib = 0 } else { ib = parseInt((i.length * 100) / efect); }
    if (o == null) { ob = 0 } else { ob = parseInt((o.length * 100) / efect); }
    if (u == null) { ub = 0 } else { ub = parseInt((u.length * 100) / efect); }
    imprimir();
}

function imprimir() {

    document.querySelector(".total2").innerHTML = "Total de caracteres de la frase-- " + efect;

    if (a < 1) { document.querySelector(".aaa2").innerHTML = "No hay ninguna en la frase" } else
        document.querySelector(".aaa2").innerHTML = "Total de -a- en la frase-- " + a.length + " --; " + ab + "% del total de caracteres";

    if (e < 1) { document.querySelector(".eee2").innerHTML = "No hay ninguna en la frase" } else
        document.querySelector(".eee2").innerHTML = "Total de -e- en la frase-- " + e.length + " --; " + eb + "% del total de caracteres";

    if (i < 1) { document.querySelector(".iii2").innerHTML = "No hay ninguna en la frase" } else
        document.querySelector(".iii2").innerHTML = "Total de -i- en la frase-- " + i.length + " --; " + ib + "% del total de caracteres";

    if (o < 1) { document.querySelector(".ooo2").innerHTML = "No hay ninguna en la frase" } else
        document.querySelector(".ooo2").innerHTML = "Total de -o- en la frase-- " + o.length + " --; " + ob + "% del total de caracteres";

    if (u < 1) { document.querySelector(".uuu2").innerHTML = "No hay ninguna en la frase" } else
        document.querySelector(".uuu2").innerHTML = "Total de -u- en la frase-- " + u.length + " --; " + ub + "% del total de caracteres";
}